#ifndef TST_TEST_VVID_H
#define TST_TEST_VVID_H

#include <QtCore>
#include <QtTest/QtTest>

class test_vvid : public QObject
{
    Q_OBJECT

public:
    test_vvid();

private slots:
    void testLinear();
    void testSquare();
    void testCube();
    void testElemA();
    void testSumA();
    void testSumG();

};

#endif // TST_TEST_VVID_H
