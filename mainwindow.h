#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "functions.h"
#include <QMainWindow>
#include <QDateTime>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_linear_pushButton_clicked();

    void on_sqr_pushButton_clicked();

    void on_cube_pushButton_clicked();

    void on_arithm_elem_pushButton_clicked();

    void on_arithm_sum_pushButton_clicked();

    void on_geom_sum_pushButton_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
